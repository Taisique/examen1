﻿namespace Examen1
{
    class clsCliente
    {
        //properties class        
        public string nombre { get; set; }
        public int[] compras { get; set; }

        //default constructor
        public clsCliente() { }

        //customize constructor
        public clsCliente(string nombre,  int[] compras)
        {
            this.nombre = nombre;
            this.compras = compras;
        }
    }
}
