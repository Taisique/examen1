﻿using System;
    using System.Threading;
    using Examen1;

Console.WriteLine("");
Console.SetCursorPosition(2, 2);
Console.Write("Tai numberWan");
Random rand = new Random();
//creates customers instance

clsCliente cliente1 = new("Morticia",
                           new int[] { rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99)});
clsCliente cliente2 = new("PataC Hueca",
                           new int[] { rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99)});

clsCliente cliente3 = new("Nofu Me",
                           new int[] { rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99)});
clsCliente cliente4 = new("Hector",
                           new int[] { rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99)});
clsCliente cliente5 = new("Polo North",
                           new int[] { rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99)});
clsCliente cliente6 = new("Polo South"  ,
                           new int[] { rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99)});
clsCliente cliente7 = new("Pali toseco",
                           new int[] { rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99)});
clsCliente cliente8 = new("Tristana",
                           new int[] { rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99)});
clsCliente cliente9 = new("Portones",
                           new int[] { rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99),
                                       rand.Next(0, 99)});

//creates cashiers instance
clasCajero[] cajeros = new clasCajero[10];

cajeros[0] = new ("Herman", cliente1, 1, ConsoleColor.Yellow);
cajeros[1] = new ("Lillyth", cliente2, 18, ConsoleColor.Magenta);
cajeros[2] = new("Monica", cliente3, 36, ConsoleColor.Red);
cajeros[3] = new("Carlitos", cliente4, 54, ConsoleColor.Green);
cajeros[4] = new("Yoel", cliente5, 72, ConsoleColor.Green);
cajeros[5] = new("Juan", cliente6, 90, ConsoleColor.Green);
cajeros[6] = new("Moiso", cliente7, 108, ConsoleColor.Green);
cajeros[7] = new("Kalo", cliente8, 126, ConsoleColor.Green);
cajeros[8] = new("Kilom", cliente9, 144, ConsoleColor.Green);


//create thread for each job
Thread hilo1 = new(cajeros[0].procesarCompra);
Thread hilo2 = new(cajeros[1].procesarCompra);
Thread hilo3 = new(cajeros[2].procesarCompra);
Thread hilo4 = new(cajeros[3].procesarCompra);
Thread hilo5 = new(cajeros[4].procesarCompra);
Thread hilo6 = new(cajeros[5].procesarCompra);
Thread hilo7 = new(cajeros[6].procesarCompra);
Thread hilo8 = new(cajeros[7].procesarCompra);
Thread hilo9 = new(cajeros[8].procesarCompra);


    //starts jobs
hilo1.Start();
hilo2.Start();
hilo3.Start();
hilo4.Start();
hilo5.Start();
hilo6.Start();
hilo7.Start();
hilo8.Start();
hilo9.Start();


//runs while some task is alive
while ((hilo1.IsAlive) || (hilo2.IsAlive || hilo3.IsAlive || hilo4.IsAlive || hilo5.IsAlive || hilo6.IsAlive || hilo7.IsAlive
    || hilo8.IsAlive || hilo9.IsAlive)) {

}

//prints the both task time
if ((!hilo1.IsAlive) && (!hilo2.IsAlive) && (!hilo3.IsAlive) && (!hilo4.IsAlive) && (!hilo4.IsAlive) && (!hilo4.IsAlive) && (!hilo4.IsAlive)
    && (!hilo4.IsAlive) && (!hilo4.IsAlive))
{
    int a = rand.Next(1, 10);
    int b = rand.Next(1, 10);
    int c = rand.Next(1, 10);
    int d = rand.Next(1, 10);
    int e = rand.Next(1, 10);
    int f = rand.Next(1, 10);
    Console.SetCursorPosition(0, 25);
    Console.WriteLine("{0, -26} ║ {1, 12} ║ {2, 7} ║ {3, 10}", "Premios", "Ganador", "Fracciones", "Monto");
    Console.WriteLine("{0, -8} ║ {1, 15} ║ {2, 11}  ║ {3, 10} ║ {4, 8}", "Primer", "100,000,000.00", cliente1.nombre, a, (100000000 / 10) * a);
    Console.WriteLine("{0, -8} ║ {1, 15} ║ {2, 11}  ║ {3, 10} ║ {4, 8}", "Segundo", "50,000,000.00", cliente3.nombre, b, (50000000 / 10) * b);
    Console.WriteLine("{0, -8} ║ {1, 15} ║ {2, 11}  ║ {3, 10} ║ {4, 8}", "Tercero", "25,000,000.00", cliente5.nombre, c, (25000000 / 10) * c);
    Console.WriteLine("{0, -8} ║ {1, 15} ║ {2, 11}  ║ {3, 10} ║ {4, 8}", "Cuarto", "12,500,000.00", cliente2.nombre, d, (12500000 / 10) * d);
    Console.WriteLine("{0, -8} ║ {1, 15} ║ {2, 11}  ║ {3, 10} ║ {4, 8}", "Quinto", "6,250,000.00", cliente4.nombre, e, (6250000 / 10) * e);
    Console.WriteLine("{0, -8} ║ {1, 15} ║ {2, 11}  ║ {3, 10} ║ {4, 8}", "Sexto", "3,125,000.00", cliente8.nombre, f, (3125000 / 10) * f);
}

// Await the keypress from user
Console.Write("\n\nPresione cualquier tecla para continuar...........");
Console.ReadKey();


