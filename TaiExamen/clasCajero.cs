﻿using System;
using System.Threading;

namespace Examen1
    {
    class clasCajero
    {
        private string nombre;
        private clsCliente cliente;
        private int colu;

        private DateTime TiempoI = DateTime.Now;
        private DateTime TiempoF = DateTime.Now;

        private System.ConsoleColor color = ConsoleColor.White;

        public string TiempoTotal()
        {
            return (TiempoF - TiempoI).TotalSeconds.ToString();
        }

        //default constructor
        public clasCajero() { }

        //custom constructor
        public clasCajero(string nomb, clsCliente clie, int col, System.ConsoleColor colo)
        {
            this.nombre = nomb;
            this.cliente = clie;
            this.colu = col;
            this.color = colo;
        }

        //delay run applications any seconds
        private void esperarXsegundos(int segundos)
        {
            try
            {
                Thread.Sleep(segundos * 1000);
            }
            catch (ThreadInterruptedException ex)
            {
                Console.WriteLine(ex.Message);
                Thread.CurrentThread.Interrupt();
            }
        }

        //simulates the a cashier process when apply the customer sales
        public void procesarCompra()
        {
            Random rnd = new Random();
            int fila = 5;
            var inicio = DateTime.Now;
            TiempoI = DateTime.Now;

            this.esperarXsegundos(rnd.Next(2) + 1);

            //prints order header
            Console.ForegroundColor = color;
            Console.SetCursorPosition(colu, fila++); Console.Write("Puesto: " + this.nombre); 
            Console.SetCursorPosition(colu, fila++); Console.Write("Inicio: " + inicio.ToString("hh:mm:ss"));
            Console.ForegroundColor = ConsoleColor.White;

            this.esperarXsegundos(rnd.Next(3) + 1);

            var durante1 = inicio;

            //prints order details
            Console.SetCursorPosition(colu, fila++);
            Console.Write("{0,-5}", "Numero");

            Console.SetCursorPosition(colu, fila++);
            Console.Write("---+---");

            foreach (int compra in cliente.compras)
            {
                this.esperarXsegundos(rnd.Next(3) + 1);
                var durante2 = DateTime.Now;

                Console.SetCursorPosition(colu, fila++);
                Console.Write("{0,-5}", compra);

                Console.SetCursorPosition(colu, fila++);
                Console.Write("---+---");

                durante1 = DateTime.Now;
            }
        } // end procesarCompra function
    }
}

